resource "aws_s3_bucket_object" "frontend_object" {
  depends_on = [aws_s3_bucket.main]
  for_each = fileset("${local.source_folder}/", "**/*")
  key    = each.value
  source =  "${local.source_folder}/${each.value}"
  bucket = aws_s3_bucket.main.bucket
  etag         = filemd5("${local.source_folder}/${each.value}")
  content_type = lookup(local.mime_type_mappings, concat(regexall("\\.([^\\.]*)$", each.value), [[""]])[0][0], "application/octet-stream")
}

resource "aws_s3_bucket_object" "config_json_file" {
  provider = aws
  depends_on = [aws_s3_bucket_object.frontend_object]
  bucket = aws_s3_bucket.main.bucket
  key = local.build_config_path
  content_type = "text/json"
  content  = var.config_json_content
}
