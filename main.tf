
terraform {
   required_version = ">= 0.13"

  required_providers {
    atn-utils = {
      source = "allence-tunisie/atn-utils"
    }
    aws = {
      source  = "hashicorp/aws"
      version = "> 3.70.0"
    }
  }
}


