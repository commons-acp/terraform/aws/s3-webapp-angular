variable "app_name" {
  description = ""
}
variable "allowed_ips" {
  default = [
    "0.0.0.0/0"            # public access
    # "xxx.xxx.xxx.xxx/mm" # restricted
    # "999.999.999.999/32" # invalid IP, completely inaccessible
  ]
}


variable "routing_rules" {
  type        = string
  description = "Routing rules for the S3 bucket"
  default     = ""
}
variable "refer_secret" {
  type        = string
  description = "A secret string to authenticate CF requests to S3"
  default     = "123-VERY-SECRET-123"
}
variable "config_json_content" {
  description = "contenue du fichier du config"
}

variable "gitlab_package_url" {
  description = "gitlab package url"
}
variable "gitlab_api_token" {
  description = "gitlab api token"
}
